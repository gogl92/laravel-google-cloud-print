<?php

return [
    //file path of client credentials from google developers console download
    'credentials' => 'client_secrets.json',
    //Type of access to request. Offline access will use a refresh token so user input is no longer required for authorizing requests.
    'access_type' => 'offline',
    //Force approval prompt on every authorization or not
    'approval_prompt' => 'force',
];