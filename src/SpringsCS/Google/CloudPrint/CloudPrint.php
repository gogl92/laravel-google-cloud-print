<?php

namespace SpringsCS\Google\CloudPrint;

use Illuminate\Contracts\Config\Repository as Config;

class CloudPrint extends Api
{
    /**
     * Application configuration
     *
     * @var Config
     */
    protected $config;

    /**
     * CloudPrint constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        parent::__construct($this->config->get('cloud-print'));

        return $this;
    }

    /**
     * Creates an HTML print job
     *
     * @param string $html
     * @return Task
     */
    public function html($html = '')
    {
        return Task::make($this, 'text/html')
            ->content($html);
    }

    /**
     * Create a file print job
     *
     * @param string $path
     * @return Task
     */
    public function file($path)
    {
        return Task::make($this)
            ->file($path);
    }

    /**
     * Create a raw print job
     *
     * @param mixed $raw
     * @return Task
     */
    public function raw($raw = null)
    {
        return Task::make($this)
            ->content($raw);
    }

    /**
     * Create a text print job
     *
     * @param $text
     * @return Task
     */
    public function text($text = '')
    {
        return Task::make($this, 'text/plain')
            ->content($text);
    }

    /**
     * Create a url print job
     *
     * @param $url
     * @return Task
     */
    public function url($url)
    {
        return Task::make($this)
            ->url($url);
    }
}