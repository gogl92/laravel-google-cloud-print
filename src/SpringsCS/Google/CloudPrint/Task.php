<?php

namespace SpringsCS\Google\CloudPrint;

use SpringsCS\Google\CloudPrint\Exception\InvalidSourceException;
use SpringsCS\Google\CloudPrint\Exception\UnauthorizedAccessException;
use SpringsCS\Google\CloudPrint\Exception\UnknownPrinterException;

class Task {

    /**
     * ID of the printer to print this job on
     *
     * @var string
     */
    protected $printer;

    /**
     * Content Type of the job
     *
     * @var string
     */
    protected $contentType;

    /**
     * Title of the job
     *
     * @var string
     */
    protected $title;

    /**
     * Tags for the job
     *
     * @var array
     */
    protected $tags = [];

    /**
     * Content to print
     *
     * @var string
     */
    protected $source = '';

    /**
     * Print job options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Cloud Print API Instance
     *
     * @var Api
     */
    protected $api;

    /**
     * Base64 encode job
     *
     * @var bool
     */
    protected $base64 = true;

    /**
     * Response
     *
     * @var \stdClass
     */
    protected $response;

    /**
     * CloudPrintJob constructor
     *
     * @param Api $api
     * @param $contentType
     */
    public function __construct(Api $api, $contentType = null)
    {
        $this->contentType = $contentType;
        $this->api = $api;

        return $this;
    }

    public static function make(Api $api, $contentType = null)
    {
        return new Task($api, $contentType);
    }

    //region Helper Methods

    /**
     * Build ticket for job submission
     *
     * @return array
     */
    protected function buildTicket()
    {
        $t = [
            'version' => '1.0',
        ];

        if (!empty($this->options)) {
            $t['print'] = $this->options;
        }

        return $t;
    }

    /**
     * Build content
     *
     * @return string
     */
    protected function buildContent()
    {
        return $this->base64 ? base64_encode($this->source) : $this->source;
    }

    /**
     * Build job data
     *
     * @return array
     */
    protected function buildData()
    {
        return [
            'title' => $this->title,
            'contentTransferEncoding' => 'base64',
            'content' => $this->buildContent(),
            'contentType' => $this->contentType,
            'ticket' => $this->buildTicket(),
            'tag' => $this->buildTags()
        ];
    }

    /**
     * Build tags string for job submission
     *
     * @return string
     */
    protected function buildTags()
    {
        return join(',', $this->tags);
    }

    //endregion

    //region Content Types

    /**
     * Set the content of the job, this is what will be printed
     *
     * @param $raw
     * @return $this
     */
    public function content($raw)
    {
        $this->source = $raw;

        return $this;
    }

    /**
     * Select a file path to print
     *
     * @param $file
     * @return $this
     * @throws InvalidSourceException
     */
    public function file($file)
    {
        if (!file_exists($file)) {
            throw new InvalidSourceException();
        }

        $this->source = file_get_contents($file);

        return $this;
    }

    /**
     * Download the content from a URL to print
     *
     * @param $url
     * @return $this
     * @throws InvalidSourceException
     */
    public function url($url)
    {
        if ( ! preg_match('/^https?:\/\//', $url)) {
            throw new InvalidSourceException();
        }

        $this->source = file_get_contents($url);

        return $this;
    }

    /**
     * Set the title of the job
     *
     * @param $title
     * @return $this
     */
    public function title($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Set the content type of the job
     *
     * @param $type
     * @return $this
     */
    public function type($type)
    {
        $this->contentType = $type;

        return $this;
    }

    //endregion

    /**
     * Set the printer to use for the job
     *
     * @param $printer
     * @return $this
     */
    public function printer($printer)
    {
        $this->printer = $printer;

        return $this;
    }

    /**
     * Set a tag or tags
     *
     * @param $tags
     * @return $this
     */
    public function tags($tags)
    {
        if (!is_array($tags)) {
            $this->tags[] = $tags;
        } elseif (func_num_args() > 1) {
            $this->tags = func_get_args();
        } else {
            $this->tags = $tags;
        }

        $this->tags = array_map('str_slug', $this->tags);

        return $this;
    }

    //region Job Options

    /**
     * Collates the print job
     *
     * @param bool $f
     * @return $this
     */
    public function collate($f = true)
    {
        $this->ticket('collate', ['collate' => $f]);

        return $this;
    }

    /**
     * Set the color to use for the job
     *
     * @param mixed $type
     * @param mixed $vendor
     * @return $this
     */
    public function color($type, $vendor = null)
    {
        $opps = [
            'type' => $type
        ];

        if (!empty($vendor)) {
            $opts['vendor_id'] = $vendor;
        }

        $this->ticket('color', $opts);

        return $this;
    }

    /**
     * Sets the number of copies to print for the job
     *
     * @param int $n
     * @return $this
     */
    public function copies($n)
    {
        $this->ticket('copies', ['copies' => $n]);

        return $this;
    }

    /**
     * Sets the duplex type to use for the job
     *
     * @param mixed $type
     * @return $this
     */
    public function duplex($type)
    {
        $this->ticket('duplex', ['type' => $type]);

        return $this;
    }

    /**
     * Sets the output orientation of the page
     *
     * @param mixed $type
     * @return $this
     */
    public function orient($type)
    {
        $this->ticket('page_orientation', ['type' => $type]);

        return $this;
    }

    /**
     * Set the margins in millimeters. Works like CSS margins
     *
     * @param integer $top
     * @param null|integer $right
     * @param null|integer $bottom
     * @param null|integer $left
     * @param bool $cm
     * @return $this
     */
    public function margin($top, $right = null, $bottom = null, $left = null, $cm = false)
    {
        // mm or cm
        $mul = $cm ? 100000 : 1000;

        if (is_null($right)) {
            $right = $top;
        }

        if (is_null($bottom)) {
            $bottom = $top;
        }

        if (is_null($left)) {
            $left = $right;
        }

        $this->ticket('margins', [
            'top_microns' => $top * $mul,
            'right_microns' => $right * $mul,
            'bottom_microns' => $bottom * $mul,
            'left_microns' => $left * $mul,
        ]);

        return $this;
    }

    /**
     * Set the margins in centimeters
     *
     * @param integer $top
     * @param null|integer $right
     * @param null|integer $bottom
     * @param null|integer $left
     * @return Task
     */
    public function marginCm($top, $right = null, $bottom = null, $left = null)
    {
        return $this->margin($top, $right, $bottom, $left);
    }

    /**
     * Set the start and end page range to print
     *
     * @param $start
     * @param $end
     * @return $this
     */
    public function range($start, $end)
    {
        if ($start > $end) {
            $tmp = $start;
            $start = $end;
            $end = $tmp;
        }

        $this->ticket('', [
            'interval' => [
                [ 'start' => $start, 'end' => $end ]
            ]
        ]);

        return $this;
    }

    /**
     * Reverse the order of the print
     *
     * @param bool $f
     * @return $this
     */
    public function reverse($f = true)
    {
        $this->ticket('reverse_order', ['reverse_order' => $f]);

        return $this;
    }

    //endregion

    /**
     * Set options on the job ticket
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function ticket($key, $value)
    {
        $this->options[$key] = $value;

        return $this;
    }

    /**
     * Alias to ticket()
     *
     * @param $key
     * @param $value
     * @return Task
     */
    public function option($key, $value)
    {
        return $this->ticket($key, $value);
    }

    /**
     * Configure job to base64 encode content
     *
     * @param $f
     * @return $this
     */
    public function base64($f)
    {
        $this->base64 = $f;

        return $this;
    }

    /**
     * Get the job
     *
     * @return Job
     */
    public function job()
    {
        return new Job($this->api, $this->response->job);
    }

    /**
     * Get the response from the job
     *
     * @return \stdClass
     */
    public function response()
    {
        return $this->response;
    }

    /**
     * Send the job off for printing
     * @return Task
     * @throws UnauthorizedAccessException
     * @throws UnknownPrinterException
     */
    public function send()
    {
        // Was a printer set?
        if (empty($this->printer)) {
            throw new UnknownPrinterException('Printer not specified');
        }

        // Try to automatically detect the mime type, this only works if
        // file info is loaded
        if (empty($this->contentType) && extension_loaded('fileinfo')) {
            $f = new \finfo(FILEINFO_MIME_TYPE);
            $this->contentType = $f->buffer($this->source);
        }

        $this->response = $this->api->submit($this->printer, $this->buildData());

        // If the job errored, is it because we have not accepted the printer?
        // if so, auto accept in the invite
        if (!$this->response->success && $this->response->errorCode == 8) {
            $invite = $this->api->processInvite($this->printer);
            // If we were able to accept, re-send job
            if ($invite->success == true) {
                return $this->send();
            }

            throw new UnauthorizedAccessException('Unauthorized to print on this printer');
        }

        return $this;
    }
}