<?php

namespace SpringsCS\Google\CloudPrint\Facade;

use Illuminate\Support\Facades\Facade;

class CloudPrint extends Facade
{
    /**
     * Get the registered name of the print component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'cloudprint';
    }
}