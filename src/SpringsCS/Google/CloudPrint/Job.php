<?php

namespace SpringsCS\Google\CloudPrint;

use SpringsCS\Google\CloudPrint\Exception\UnknownJobException;

class Job
{
    /**
     * Job Data
     *
     * @var array|\stdClass
     */
    protected $data;
    protected $api;

    public function __construct(Api $api, $job)
    {
        $this->data = $job;
        $this->api = $api;
    }

    /**
     * Magic method for retrieving data
     *
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (isset($this->data->{$attribute})) {
            return $this->data->{$attribute};
        }

        return null;
    }

    /**
     * Get job ID
     *
     * @return mixed
     * @throws UnknownJobException
     */
    public function getId()
    {
        if (!isset($this->data->id) || empty($this->data->id)) {
            throw new UnknownJobException('Job ID not specified');
        }

        return $this->data->id;
    }

    /**
     * Refresh job status
     */
    public function refresh()
    {
        $response = $this->api->job($this->getId());
        $this->data = $response->job;
    }

    /**
     * Cancel the print job
     */
    public function cancel()
    {
        return $this->api->deleteJob($this->getId());
    }

    /**
     * Retrieve the status of the job (helper method). Also allows for refresh
     *
     * @param bool $refresh
     * @return mixed
     */
    public function status($refresh = false)
    {
        if ($refresh) {
            $this->refresh();
        }

        return $this->status;
    }
}