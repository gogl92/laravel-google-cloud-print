<?php

if ( ! function_exists('cloudprint')) {
    /**
     * Initiate CAS hook.
     *
     * @return \SpringsCS\Google\CloudPrint\CloudPrint
     */
    function cloudprint() {
        return app('cloudprint');
    }
}