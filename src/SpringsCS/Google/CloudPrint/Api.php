<?php

namespace SpringsCS\Google\CloudPrint;

use Google_Client;
use GuzzleHttp\Client;
use SpringsCS\Google\CloudPrint\Exception\InvalidCredentialsException;

class Api
{
    /**
     * Application configuration
     *
     * @var array
     */
    protected $config;

    /*
     * Google Client
     *
     * @var Google_Client
     */
    protected $client;

    /**
     * Google Client Access token
     *
     * @var string
     */
    protected $accessToken;

    /**
     * Default configuration
     *
     * @var array
     */
    protected $configDefaults = [
        'access_type' => 'offline',
        'approval_prompt' => 'force',
        'scopes' => [
            "https://www.googleapis.com/auth/cloudprint"
        ],
        'api_urls' => [
            'delete' => 'https://www.google.com/cloudprint/delete',
            'deletejob' => 'https://www.google.com/cloudprint/deletejob',
            'job' => 'https://www.google.com/cloudprint/job',
            'jobs' => 'https://www.google.com/cloudprint/jobs',
            'printer' => 'https://www.google.com/cloudprint/printer',
            'printers' => 'https://www.google.com/cloudprint/list',
            'processinvite' => 'https://www.google.com/cloudprint/processinvite',
            'search' => 'https://www.google.com/cloudprint/search',
            'submit' => 'https://www.google.com/cloudprint/submit',
        ]
    ];

    /**
     * HTTP Client
     *
     * @var Client
     */
    private $baseHttpClient;

    /**
     * Response Object
     *
     * @var \stdClass
     */
    private $response;

    /**
     * CloudPrintApi constructor
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = array_merge($this->configDefaults, $config);
    }

    /**
     * Configure the HTTP Client
     *
     * @throws InvalidCredentialsException
     */
    protected function setupClient()
    {
        if ($this->client !== null)
            return;

        if (!array_key_exists('credentials', $this->config)) {
            throw new InvalidCredentialsException('Required Google Client configuration file not specified');
        }

        $this->client = new Google_Client();
        $this->client->setAuthConfig($this->config['credentials']);

        foreach ($this->config['scopes'] as $scope) {
            $this->client->addScope($scope);
        }
    }

    /**
     * Format the response
     *
     * @return mixed
     */
    protected function getResponse()
    {
        return json_decode($this->response->getBody());
    }

    /**
     * Create HTTP Client with token
     *
     * @return Client
     */
    public function getBaseHttpClient()
    {
        $this->baseHttpClient = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAccessToken()
            ]
        ]);

        return $this->baseHttpClient;
    }

    /**
     * Get endpoint URL
     *
     * @param $key
     * @return mixed
     */
    public function getUrl($key)
    {
        if (isset($this->config['api_urls'][$key])) {
            return $this->config['api_urls'][$key];
        }

        return $key;
    }

    /**
     * Get OAuth Access Token
     *
     * @return string
     * @throws InvalidCredentialsException
     */
    public function getAccessToken()
    {
        $this->setupClient();

        if ($this->client->isAccessTokenExpired()) {
            $this->client->fetchAccessTokenWithAssertion();
        }

        if (!($accessToken = $this->client->getAccessToken())) {
            throw new InvalidCredentialsException('Invalid access token');
        }

        $this->accessToken = $accessToken['access_token'];
        return $this->accessToken;
    }

    /**
     * Call an api endpoint
     *
     * @param $service
     * @param array $data
     * @param array $headers
     * @return mixed|\Psr\Http\Message\ResponseInterface|\stdClass
     */
    public function call($service, $data = [], $headers = [])
    {
        $client = $this->getBaseHttpClient();

        $multipart = [];
        foreach ($data as $k => $v) {
            $multipart[] = [
                'name' => $k,
                'contents' => $v
            ];
        }

        $this->response = $client->request('POST', $this->getUrl($service), [
            'multipart' => $multipart,
            'headers' => $headers
        ]);

        return $this->response;
    }

    /**
     * Delete Printer by ID
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->call('delete', ['printerid' => $id]);

        return $this->getResponse();
    }

    /**
     * Delete Job by ID
     *
     * @param $id
     * @return mixed
     */
    public function deleteJob($id)
    {
        $this->call('deletejob', ['jobid' => $id]);

        return $this->getResponse();
    }

    /**
     * Get Job by ID
     *
     * @param $id
     * @param array $params
     * @return mixed
     */
    public function job($id, $params = [])
    {
        $params['jobid'] = $id;

        $this->call('job', $params);

        return $this->getResponse();
    }

    /**
     * Get all jobs
     *
     * @param array $params
     * @return mixed
     */
    public function jobs($params = [])
    {
        $this->call('jobs', $params);

        return $this->getResponse();
    }

    /**
     * Get a printer
     *
     * @param $printerid
     * @param string $client
     * @param string $extra_fields
     * @return mixed
     */
    public function printer($printerid, $client = '', $extra_fields = '')
    {
        $this->response = $this->call('printer', [
            'printerid' => $printerid,
            'extra_fields' => $extra_fields,
            'client' => $client
        ]);

        return $this->getResponse();
    }

    /**
     * Get all printers for a proxy. Use search to get all printer for a user
     *
     * @param $proxy
     * @param string $extra_fields
     * @return mixed
     */
    public function printers($proxy, $extra_fields = '')
    {
        $this->response = $this->call('list', ['proxy' => $proxy, 'extra_fields' => $extra_fields]);

        return $this->getResponse();
    }

    /**
     * Process an invite request for a printer
     *
     * @param $printer
     * @param string $accept
     * @return mixed
     */
    public function processInvite($printer, $accept = 'true')
    {
        $data = [
            'printerid' => $printer,
            'accept' => $accept
        ];

        $this->call('processinvite', $data);

        return $this->getResponse();
    }

    /**
     * Perform a search on printers and jobs
     *
     * @param string $query
     * @param array $data
     * @return mixed
     */
    public function search($query = '', $data = [])
    {
        $data['q'] = $query;
        $this->response = $this->call('search', $data);

        return $this->getResponse();
    }

    /**
     * Submit a new print job
     *
     * @param $printer
     * @param array $data
     * @param array $headers
     * @return mixed
     */
    public function submit($printer, $data = [], $headers = [])
    {
        $data['printerid'] = $printer;
        $data['ticket'] = json_encode($data['ticket']);

        if (empty($data['title'])) {
            $data['title'] = sprintf('job-%s-%s', date('YmdHis'), rand(1000, 9999));
        }

        $this->call('submit', $data, $headers);

        return $this->getResponse();
    }
}