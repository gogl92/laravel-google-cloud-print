<?php

namespace SpringsCS\Google\CloudPrint;

use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{
    /**
     * Indicates if loading of the provider is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Boot the service provider
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../../config/config.php' => config_path('cloud-print.php')
        ]);
    }

    /**
     * Register the service provider
     */
    public function register()
    {
        $this->mergeConfigFrom( __DIR__.'/../../../config/config.php', 'cloud-print');
        $this->app->singleton('cloudprint', function ($app) {
            return new CloudPrint($app['config']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['cloudprint'];
    }
}