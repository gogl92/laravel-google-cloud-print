<?php

use Illuminate\Support\Facades\Config;
use SpringsCS\Google\CloudPrint\CloudPrint;

class CloudPrintTest extends PHPUnit_Framework_TestCase
{
    protected $config;

    public function setup()
    {
        /*$this->config = [
            'credentials' => __DIR__.'/'.getenv('GCP_TEST_CREDENTIALS')
        ];*/
        $this->config = Mockery::mock(Illuminate\Config\Repository::class);
    }

    private function getService()
    {
        $this->config->shouldReceive('get')
            ->with('cloud-print')
            ->once()
            ->andReturn([
                'credentials' => __DIR__.'/'.getenv('GCP_TEST_CREDENTIALS')
            ]);

        return new CloudPrint($this->config);
    }

    private function getPrinter()
    {
        return getenv('GCP_TEST_PRINTER');
    }

    /**
     * @test
     */
    public function it_authenticates()
    {
        $gcp = $this->getService();

        $this->assertNotEmpty($gcp->getAccessToken());
    }

    /**
     * @test
     */
    public function it_lists_printers()
    {
        $gcp = $this->getService();

        $s = $gcp->search();
        $this->assertObjectHasAttribute('success', $s);
        $this->assertTrue($s->success);
    }

    /**
     * @test
     */
    public function it_prints()
    {
        $gcp = $this->getService();
        $printer = $this->getPrinter();

        $task = $gcp->text()
            ->content('This is a test from PHP')
            ->printer($printer)
            ->send();

        $this->assertNotNull($task);

        $job = $task->job();
        $response = $task->response();

        $this->assertNotNull($job);
        $this->assertObjectHasAttribute('success', $response);
        $this->assertTrue($response->success);
        $this->assertEquals('QUEUED', $job->status);
    }

    /**
     * @test
     */
    public function it_prints_url()
    {
        $gcp = $this->getService();
        $printer = $this->getPrinter();

        $task = $gcp->url('https://google.com')
            ->printer($printer)
            ->send();

        $this->assertNotNull($task);

        $job = $task->job();
        $response = $task->response();

        $this->assertNotNull($job);
        $this->assertObjectHasAttribute('success', $response);
        $this->assertTrue($response->success);
        $this->assertEquals('QUEUED', $job->status);
    }
}