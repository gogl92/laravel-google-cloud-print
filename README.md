# Cloud Print

A Google Cloud Print library for Laravel 5.x

The library is an implementation of the [Google Cloud Print API.](https://developers.google.com/cloud-print/docs/appInterfaces)

## Features

- Supports server-to-server OAuth using client-secret.json
- Automatic content type detection (can be ignored if preferred). Requires fileinfo extension.
- Supports duplex, range, margins, grayscale, and all other GCP supported options. 
- Supports Laravel 5.x (tested on 5.4)
- Supports PHP 7.x
- Automatic printer invite acceptance on job submit.

[Checkout the wiki for more information](https://gitlab.com/SpringsCS/laravel-google-cloud-print/wikis/installation)